export default {
	collectCoverage: false,
	collectCoverageFrom: ['./src/**'],
	coveragePathIgnorePatterns: ['/node_modules/'],
	coverageThreshold: {
		global: { lines: 90 }
	},
	moduleFileExtensions: ['js', 'json', 'ts'],
	preset: 'ts-jest',
	roots: ['<rootDir>/src/'],
	transform: {
		'^.+\\.ts?$': 'ts-jest',
		'^.+\\.js$': 'babel-jest'
	},
	transformIgnorePatterns: ['node_modules/(?!(nanoid)/)']
};
