const {
	MAX_VALUE,
	MIN_VALUE
} = Number;

export const randomNumber = (
	min: number = MIN_VALUE,
	max: number = MAX_VALUE
): number => {
	let [safeMin, safeMax] = [min, max];

	if (min < MIN_VALUE) safeMin = MIN_VALUE;
	if (max > MAX_VALUE) safeMax = MAX_VALUE;

	return (Math.random() * (safeMax - safeMin + 1)) + safeMin;
};
