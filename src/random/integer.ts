const {
	MAX_SAFE_INTEGER,
	MIN_SAFE_INTEGER
} = Number;

export const randomInteger = (
	min: number = MIN_SAFE_INTEGER,
	max: number = MAX_SAFE_INTEGER
): number => {
	let [safeMin, safeMax] = [min, max];

	if (min < MIN_SAFE_INTEGER) safeMin = MIN_SAFE_INTEGER;
	if (max > MAX_SAFE_INTEGER) safeMax = MAX_SAFE_INTEGER;

	safeMin = Math.ceil(safeMin);
	safeMax = Math.floor(safeMax);

	return Math.floor((Math.random() * (safeMax - safeMin + 1)) + safeMin);
};
