import { randomInteger } from './integer';

export const randomFromArray = <T> (arr: readonly T[]): T => {
	const min = 0;
	const max = arr.length - 1;

	const index = randomInteger(min, max);

	return arr[index];
};
