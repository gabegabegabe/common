import { nanoid } from 'nanoid';

/**
 * Generates a unique string ID
 *
 * @param size - The size of the ID; the default is 21
 *
 * @returns The unique string ID
 *
 * @public
 */
export const uuid = (size?: number): string => nanoid(size);
