const scheduler = typeof setImmediate === 'function'
	? setImmediate
	: setTimeout;

// eslint-disable-next-line promise/avoid-new
export const flushPromises = async (): Promise<void> => new Promise(resolve => {
	scheduler(resolve);
});
