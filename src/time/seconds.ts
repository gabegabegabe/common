export const seconds = (amount: number): number => amount * 1000;
