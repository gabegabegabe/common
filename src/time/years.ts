export const years = (amount: number): number => amount * 31557600000;
