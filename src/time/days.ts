export const days = (amount: number): number => amount * 86400000;
