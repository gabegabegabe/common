export const exists = <T> (item?: T | null): item is T => item !== null && typeof item !== 'undefined';
