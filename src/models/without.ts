/**
 * A utility type that requires all fields defined in T but not in U to be
 * `never`
 *
 * @remarks Credit to this TS issue for the implementation:
 * https://github.com/Microsoft/TypeScript/issues/1409
 *
 * @public
 */
// eslint-disable-next-line @typescript-eslint/consistent-indexed-object-style
export type Without<T, U> = {
	[P in Exclude<keyof T, keyof U>]?: never;
};
