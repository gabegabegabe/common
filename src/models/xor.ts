import type { Without } from './without';

/**
 * A utility type for an exclusive or of T and U
 *
 * @remarks Credit to this TS issue for the implementation:
 * https://github.com/Microsoft/TypeScript/issues/14094
 *
 * @public
 */
export type XOR <T, U> = (T | U) extends object
	? (T & Without<U, T>) | (U & Without<T, U>)
	: T | U;
