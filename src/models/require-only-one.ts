/**
 * This utility type allows requiring exclusively one of n- specified _optional_
 * properties of the template type T.
 *
 * @example
 * ```
 * type Foo = {
 *   id: number;
 *   bar?: string;
 *   baz?: string;
 * };
 *
 * type RequiredFoo = RequireOnlyOne<Foo, 'bar' | 'baz'>;
 *
 * // These would throw type errors
 * const foo1: RequiredFoo = { id: 5 };
 * const foo2: RequiredFoo = {
 *   id: 7,
 *   bar: 'asdf',
 *   baz: 'zxcv'
 * };
 *
 * // These would not
 * const foo3: RequiredFoo = { id: 5, bar: 'asdf' };
 * const foo4: RequiredFoo = { id: 7, baz: 'zxcv' };
 * ```
 *
 * @remarks Credit to KPD on StackOverflow for this.
 * https://stackoverflow.com/a/49725198
 *
 * @public
 */
export type RequireOnlyOne<T, Keys extends keyof T = keyof T> =
	Pick<T, Exclude<keyof T, Keys>>
	& {
		[K in Keys]-?:
		Required<Pick<T, K>>
		& Partial<Record<Exclude<Keys, K>, undefined>>
	}[Keys];
