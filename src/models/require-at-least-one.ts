/**
 * This utility type allows requiring at least one of n- specified _optional_
 * properties of the template type T.
 *
 * @example
 * ```
 * type Foo = {
 *   id: number;
 *   bar?: string;
 *   baz?: string;
 * };
 *
 * type RequiredFoo = RequireAtLeastOne<Foo, 'bar' | 'baz'>;
 *
 * // These would throw type errors
 * const foo1: RequiredFoo = { id: 5 };
 *
 * // These would not
 * const foo2: RequiredFoo = { id: 5, bar: 'asdf' };
 * const foo3: RequiredFoo = { id: 7, baz: 'zxcv' };
 * const foo4: RequiredFoo = {
 *   id: 7,
 *   bar: 'asdf',
 *   baz: 'zxcv'
 * };
 * ```
 *
 * @remarks Credit to KPD on StackOverflow for this.
 * https://stackoverflow.com/a/49725198
 *
 * @public
 */
export type RequireAtLeastOne<T, Keys extends keyof T = keyof T> =
	Pick<T, Exclude<keyof T, Keys>>
	& {
		[K in Keys]-?: Required<Pick<T, K>> & Partial<Pick<T, Exclude<Keys, K>>>
	}[Keys];
