/**
 * A utility type definition that can be used to "convert" a union type to an
 * intersection type.
 *
 * @remarks Credit to jcalz on StackOverflow for this.
 * https://stackoverflow.com/a/50375286
 *
 * @public
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type UnionToIntersection <U> = (U extends any ? (k: U) => void : never) extends ((k: infer I) => void) ? I : never;
