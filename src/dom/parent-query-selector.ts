// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
export const parentQuerySelector = (element: Element, selector: string): Element | null => {
	let { parentElement: iterator } = element;

	while (iterator !== null && !iterator.matches(selector)) iterator = iterator.parentElement;

	return iterator;
};
