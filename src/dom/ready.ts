const LOADING_STATE = 'loading';
const LOADED_EVENT = 'DOMContentLoaded';

export const ready = (fn: (...args: readonly unknown[]) => unknown): void => {
	if (document.readyState === LOADING_STATE) document.addEventListener(LOADED_EVENT, fn);
	else fn();
};
