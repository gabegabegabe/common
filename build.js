import browserslist from 'browserslist';
import consola from 'consola';
import esbuild from 'esbuild';
import { esbuildPluginBrowserslist } from 'esbuild-plugin-browserslist';
import fs from 'fs/promises';
import path from 'path';

const SOURCE_DIR = 'src';
const OUT_DIR = 'dist';
const ESM_FORMAT = 'esm';
const ESM_EXTENSION = '.esm.js';
const CJS_FORMAT = 'cjs';
const CJS_EXTENSION = '.cjs.js';
const ENTRY_POINT_FILENAME = 'index.ts';

// eslint-disable-next-line no-underscore-dangle
let _pkg = null;
const getPkg = async () => {
	if (_pkg !== null) return _pkg;

	const pkgString = await fs.readFile('./package.json', 'utf-8');
	// eslint-disable-next-line require-atomic-updates
	_pkg = JSON.parse(pkgString);

	return _pkg;
};

// This function will recursively scan the 'src' directory, find any 'index.ts'
// files it finds within (entry points) and return the resulting array of
// absolute paths to said entry points.
// eslint-disable-next-line require-await
const getEntryPoints = async () => {
	const findMatches = async (dirPath, filename) => {
		const dir = await fs.opendir(dirPath);

		const matches = [];

		// eslint-disable-next-line curly
		for await (const dirent of dir) {
			if (dirent.isDirectory()) matches.push(...await findMatches(`${dirPath}/${dirent.name}`, filename));
			else if (dirent.name === filename) matches.push(`${dirPath}/${dirent.name}`);
		}

		return matches;
	};

	const start = path.resolve(SOURCE_DIR);

	return findMatches(start, ENTRY_POINT_FILENAME);
};

// A function that copies this project's package.json into the output folder so
// that it may then be published to a package repository
const writePackageJson = async entryPoints => {
	const pkg = await getPkg();
	const cwd = process.cwd();

	const exports = entryPoints
		.map(entryPoint => {
			const trimmedEntryPoint = entryPoint
				.replace(`${cwd}/${SOURCE_DIR}`, '')
				.replace(`/${ENTRY_POINT_FILENAME}`, '');

			return `.${trimmedEntryPoint}`;
		})
		.reduce((collection, entryPoint) => ({
			...collection,
			[entryPoint]: {
				import: {
					types: `${entryPoint}/index.d.ts`,
					default: `${entryPoint}/index${ESM_EXTENSION}`
				},
				require: {
					types: `${entryPoint}/index.d.ts`,
					default: `${entryPoint}/index${CJS_EXTENSION}`
				}
			}
		}), {});

	// Construct the new package.json as an object
	const newPkg = {
		author: pkg.author,
		bugs: { ...pkg.bugs },
		description: pkg.description,
		exports,
		homepage: pkg.homepage,
		license: pkg.license,
		main: `index${CJS_EXTENSION}`,
		name: pkg.name,
		repository: { ...pkg.repository },
		type: pkg.type,
		types: 'index.d.ts',
		version: pkg.version
	};

	// Copy dependencies and peerDependencies if they exist
	const depString = 'dependencies';
	const peerDepString = 'peerDependencies';

	if (depString in pkg) newPkg[depString] = pkg[depString];
	if (peerDepString in pkg) newPkg[peerDepString] = pkg[peerDepString];

	// Create the JSON string, indenting by the specified amount
	const spacesToIndent = 2;
	const newPkgString = JSON.stringify(newPkg, null, spacesToIndent);

	// Write the package.json copy to disk
	await fs.writeFile(path.resolve(`./${OUT_DIR}/package.json`), newPkgString);
};

// The main build function
const build = async () => {
	consola.info('Beginning build...');

	try {
		const [
			pkg,
			entryPoints
		] = await Promise.all([
			getPkg(),
			getEntryPoints()
		]);

		const sharedConfig = {
			bundle: true,
			entryPoints,
			external: Object.keys(pkg.peerDependencies ?? {}),
			minify: true,
			outdir: 'dist',
			plugins: [
				esbuildPluginBrowserslist(browserslist(), {
					printUnknownTargets: false
				})
			],
			sourcemap: true
		};

		consola.info('CJS build starting');

		await esbuild.build({
			...sharedConfig,
			format: CJS_FORMAT,
			outExtension: {
				'.js': CJS_EXTENSION
			}
		});

		consola.success('CJS build done');

		consola.info('ESM build starting');

		await esbuild.build({
			...sharedConfig,
			format: ESM_FORMAT,
			outExtension: {
				'.js': ESM_EXTENSION
			}
		});

		consola.success('ESM build done');

		// Then copy package.json
		consola.info('Writing package.json');

		await writePackageJson(entryPoints);

		consola.success('Wrote package.json');
		consola.success('Complete!');
	} catch (err) {
		// Log any errors
		// eslint-disable-next-line no-console
		console.error(err);

		// And exit with an error status
		process.exit(1);
	}
};

// Run build
build();
